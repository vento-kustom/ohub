<?php
	/**
	 * Tela de exibição
	 * Analista/Desenvolvedor: Rafael Macedo
	 * Contato: rafa.osbourne@gmail.com
	 * Data: 23/03/2015
	 */

	$number  = tools::getNumber($this->data[0]['address']);
	$address = tools::getLetter($this->data[0]['address']);

	$email	 = $this->data[0]['email'];
	$score   = $this->data[0]['score'];
	$ip 	 = $this->data[0]['ip'];

	$token   = $_SESSION['token'];
	$cookie  = (isset($_COOKIE['cookie_token'])) ? $_COOKIE['cookie_token'] : null ;

	if(empty($_SESSION['token']))
	{
		tools::redirect('/');
	}

	include('header.php');

	?>
	<article>
		<div class="parallax">
	        <div class="container">
				<div class="row">
					<div id="inside" class="text-center col-xs-12 col-sm-12 col-md-10 col-md-1 col-lg-10 col-lg-offset-1">
						<div class="content">
							<h1>Seja Bem vindo</h1>
							<p><strong>Seu endereço de e-mail é:</strong> <?php echo $email; ?></p>
							<p><strong>Seu endereço de e-mail tem <i>score</i>:</strong> <?php echo $score; ?></p>
							<p><strong>Seu endereço comercial é:</strong> <?php echo $address; ?></p>
							<p><strong>Seu número predial é:</strong> <?php echo $number; ?></p>
							<p><strong>Seu token gravado na sessão é: </strong><?php echo $token; ?></p>
							<p><strong>Seu token gravado nos cookies é: </strong><?php echo $cookie; ?> <small>(expira em 1 hora)</small></p>
							<p><strong>Seu endereço IP é: </strong><?php echo $ip; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>

	<?php include('footer.php'); ?>
