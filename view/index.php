<?php
	/**
	 * Tela de exibição
	 * Analista/Desenvolvedor: Rafael Macedo
	 * Contato: rafa.osbourne@gmail.com
	 * Data: 23/03/2015
	 */

	if(!session_id())
		session_start();

	session_destroy();
	include('header.php');

	?>

	<article>
		<div class="parallax">
	        <div class="container">
				<div class="row">
					<div id="index" class="text-center col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
						<form id="form-send"  method="POST" action="/index/go">
							<h1>Teste desenvolvedor Full Stack.</h1>
							<br/>
							<div class="form-group">
								<div class="col-md-5">
									<input type="text" id="email" 	 name="email" 	placeholder="Digite seu e-mail">
									<span id="ok"   class="glyphicon glyphicon-ok-circle"></span>
									<span id="fail" class="glyphicon glyphicon-remove-circle"></span>
									<span class="loading"><img src="view/assets/images/loading.gif" width="20"></span>
									<span id="mailReturn"></span>
								</div>
								<div class="col-md-5">
									<input type="text" id="address" name="address" placeholder="Digite seu endereço comercial">
								</div>
								<div class="col-md-2">
									<input type="hidden" id="score" name="score" value="">
									<button id="submit" type="submit" class="btn" disabled="disabled">Pesquisar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</article>

	<?php include('footer.php'); ?>
