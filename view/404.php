<?php
	/**
	 * Tela de exibição
	 * Analista/Desenvolvedor: Rafael Macedo
	 * Contato: rafa.osbourne@gmail.com
	 * Data: 23/03/2015
	 */

	include('header.php');

	?>
    <article>
		<div class="parallax">
	        <div class="container">
	            <div class="row">
	                <div id="not-found" class="text-center col-xs-12 col-sm-12 col-md-10 col-md-1 col-lg-10 col-lg-offset-1">
						<h1>Oops... Página não encontrada</h1>
	                    <p>Não encontramos a página que você deseja, confira que o endereço está correto ou se você tem premissão para acessa-la.</p>
	                </div>
	            <div>
	        </div>
		</div>
	</article>

    <?php include('footer.php'); ?>
