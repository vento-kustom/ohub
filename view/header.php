<!DOCTYPE html>
<html>
    <head>
        <title>oHub - Teste Desenvolvedor Full Stack | Vento - Rafael Macedo</title>
        <meta charset="utf-8">
        <meta name="viewport"       content="width=device-width, initial-scale=1">
        <meta name="author" 		content="Vento - Rafael Macedo">
        <meta name="description" 	content="Teste de admissão para a vaga de Analista/Desenvolvedor para a oHub.">
        <meta name="keywords" 		content="Rafael Macedo, Desenvolvedor Web, Desenvolvedor Mobile, Analista de Sistemas, oHub, Startup, Teste">

		<!-- Base HMTL URL -->
		<base href="<?php echo BASE_URL; ?>/" />

        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="view/assets/css/style.min.css">

    </head>
    <body>
        <header>
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="#">
                                    <img class="logo" src="view/assets/images/logo-ohub.png" alt="Teste para a oHub - Vento | Rafael Macedo">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
