$(document).ready(function()
{
    /**
     * Conexão e verificação dos e-mails do form pela Mailboxplayer.
     **/
    $('#email').on('blur', function()
    {
        $('.loading').css('display', 'inline');
        $('#fail').css('display', 'none');
        $('#ok').css('display', 'none');

        var key   = 'be6bd79887638fc9128f4bc1550b5659';
        var email = $('#email').val();

        $.ajax({ method:    'GET',
    			 url: 	   'http://apilayer.net/api/check?access_key='+ key +'&email='+ email,
    			 dataType:  'jsonp',
                 success: function(json)
            	 {
            		if(json.format_valid)
                    {
                        if(json.smtp_check)
                        {
                            $('.loading').css('display', 'none');
                            $('#ok').css('display', 'block');
                            $('#fail').css('display', 'none');
                            $('#score').val(json.score);
                            $('#submit').prop('disabled', false);
                        }
                        else
                        {
                            $('.loading').css('display', 'none');
                            $('#mailReturn').html('<label id="email-error" class="error" for="email">Esse e-mail não existe.</label>');
                            $('#fail').css('display', 'block');
                            $('#ok').css('display', 'none');
                            $('#submit').prop('disabled', true);
                        }

                    }
                    else
                    {
                        $('.loading').css('display', 'none');
                        $('#mailReturn').html('<label id="email-error" class="error" for="email">E-mail em formato inválido</label>');
                        $('#fail').css('display', 'block');
                        $('#ok').css('display', 'none');
                        $('#submit').prop('disabled', true);
                    }
                },
                error: function()
                {
                    $('.loading').css('display', 'none');
                    $('#message').addClass('alert alert-danger');
                    $('#mailReturn').html('<label id="email-error" class="error" for="email">Opss... Servidor indisponível.</label>');

                },
        });
    });

    /**
     * Validação do formulário
     **/
    // Validação do formato da data
    $.validator.addMethod('houseNumber', function(value, element)
    {
        return value.replace(/[^0-9]/g,'');
    },
    'Você precisa inserir o número predial' );

    // Validação do formulário
    $('#form-send').validate(
    {
        rules:
        {
            address: { required: true, houseNumber:  true }
        },
        messages:
        {
            address: { required: 'Endereço é um campo obrigatório' }
        }
    });
});
