#!/bin/bash

echo "#### Provisioning Vagrant - Debian-7..."

    # We don't want debconf prompts
    export DEBIAN_FRONTEND=noninteractive

echo '#### Installing bases packages'
    apt-get -y install vim software-properties-common python-software-properties build-essential git curl augeas-tools augeas-lenses

echo "#### Intalling MariaDB..."
    echo 'deb [arch=amd64] http://mirror.edatel.net.co/mariadb/repo/10.1/ubuntu trusty main' >> /etc/apt/sources.list
    apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db

    apt-get update
    apt-get install --allow-unauthenticated mariadb-server mariadb-client -y

    if [[ $? > 0 ]]
    then
        echo "## MariaDB not intalled - OH! FUCK!"
        exit
    else
        echo "### Configuring MariaDB Server"

            echo "# Go SQL Command ;)"
            while !(mysqladmin ping)
            do
               echo "... waiting for mysql ..."
            done
                echo "default_password_lifetime = 0" >> /etc/mysql/my.cnf

                # Configure MySQL Remote Accesssudo service apache2
                sed -i '/^bind-address/s/bind-address.*=.*/bind-address = 0.0.0.0/' /etc/mysql/my.cnf
                sed -i 's#/var/run/mysqld/mysqld.sock#/run/mysqld/mysqld.sock#' /etc/mysql/my.cnf

                # The Password is 'secret'
                mysql -uroot -D mysql -e "UPDATE user SET password = '*14E65567ABDB5135D0CFD9A70B3032C179A49EE7'WHERE user = 'root';"
                mysql -uroot -D mysql -e "UPDATE user SET host = '%' WHERE host = 'localhost' AND user = 'root';"
                mysql -uroot -D mysql -e "GRANT ALL ON *.* TO root@'%' IDENTIFIED BY 'secret' WITH GRANT OPTION;"
                mysql -uroot -D mysql -e "FLUSH PRIVILEGES;"

                mysql -uroot -D mysql -e "CREATE SCHEMA `db_ingressos`;"
                sudo service mysql restart
    fi

echo "#### Installing Apache2"
    apt-get update
    apt-get install apache2 -y

    echo "### Configuring Apache on Vagrant!"
        a2enmod rewrite

        chown -R vagrant:www-data /var/www
        chmod 775 -R /var/www/

        sudo service apache2 restart

echo "#### Updating PHP repository"
    add-apt-repository ppa:ondrej/php
    apt-get -y update
    apt-get -y install php5.6 php5.6-mcrypt php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip
    apt-get -y install libapache2-mod-php5.6 php-xdebug

    a2enmod php5.6
    sudo service apache2 restart

    echo "sudo service apache2 start" >> /etc/rc.local

    # Config php.ini
    sudo sed -i 's/display_errors = .*/display_errors = On/'                                /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/error_reporting = .*/error_reporting = E_ALL | E_STRICT/'                /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/memory_limit = .*/memory_limit = 128M/'                                  /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/;extension=mcrypt.so*/extension=mcrypt.so/'                              /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/suhosin.session.cryptua = .*/suhosin.session.cryptua = off/'             /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/zlib.output_compression = .*/zlib.output_compression = on/'              /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/flag session.auto_start = .*/flag session.auto_start = off/'             /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/max_execution_time = .*/max_execution_time = 18000/'                     /etc/php/5.6/apache2/php.ini
    sudo sed -i 's/;always_populate_raw_post_data = .*/always_populate_raw_post_data = -1/' /etc/php/5.6/apache2/php.ini

    # Config MySQL
    sudo sed -i 's/query_cache_type = .*/query_cache_type = 1/'     /etc/mysql/my.cnf
    sudo sed -i 's/query_cache_size = .*/query_cache_size = 32M/'   /etc/mysql/my.cnf
    sudo sed -i 's/query_cache_limit = .*/query_cache_limit = 2M/'  /etc/mysql/my.cnf

    mkdir /var/www/Log
    echo "<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName teste-vento.ohub.br

        DocumentRoot /var/www/sistema/
        ErrorLog /var/www/Log/error.log
        CustomLog /var/www/Log/access.log combined
        <Directory '/var/www/sistema/'>
            Allowoverride All
        </Directory>
    </VirtualHost>" >> /etc/apache2/sites-available/sistema.conf

    sudo a2ensite sistema.conf
    sudo service apache2 restart

echo "#### Fuck-Yeah!!! Complete!!! Uhuuuu!!! ####"
