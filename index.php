<?php

	require("config.php");
	require("classes/bootstrap.php");
	require("classes/cript.php");
	require("classes/model.php");
	require("classes/tools.php");
	require("classes/controller.php");
	require("classes/view.php");

	/** Debug Mode **/
	if(DEBUG)
	{
		ini_set('display_errors',1);
		ini_set('display_startup_erros',1);
		error_reporting(E_ALL);
	}

	$app = new Bootstrap($_SERVER['REQUEST_URI']);
?>
