<?php
	class SuccessModel extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function getList($token)
		{
			return $this->select('tb_token', array('email', 'address', 'score', 'ip'), array('token' => $token));
		}
	}
