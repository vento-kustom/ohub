<?php
	class IndexModel extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function insertData($data)
		{
			$array = array( 'token'		=> $data['token'],
							'email' 	=> $data['email'],
							'score'		=> $data['score'],
							'address'	=> $data['address'],
							'ip'		=> $_SERVER['REMOTE_ADDR'],
							'insertAt'	=> date('Y-m-d H:m:s') );

			$this->insert('tb_token', $array);
			return true;
		}
	}
