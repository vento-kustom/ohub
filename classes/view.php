<?php
	class View
	{
		protected $module;
		protected $message;
		protected $data;

		//pagination
		protected $numRow;
		protected $totalPages;
		protected $page;
		protected $method;
		protected $param;
		protected $currentPage;

		//get & set
		public function setMessage($message)
		{
			$this->message = $message;
		}
		public function getMessage()
		{
			return $this->message;
		}
		public function setData($data)
		{
			$this->data = $data;
		}
		public function getData()
		{
			return $this->data;
		}
		public function setNumRow($numRow)
		{
			$this->numRow = $numRow;
		}
		public function getNumRow()
		{
			return $this->numRow;
		}
		public function setTotalPages($totalPages)
		{
			$this->totalPages = $totalPages;
		}
		public function getTotalPages()
		{
			return $this->totalPages;
		}
		public function setpage($page)
		{
			$this->page = $page;
		}
		public function getpage()
		{
			return $this->page;
		}
		public function setMethod($method)
		{
			$this->method = $method;
		}
		public function getMethod()
		{
			return $this->method;
		}
		public function setParam($param)
		{
			$this->param = $param;
		}
		public function getParam()
		{
			return $this->param;
		}
		public function setCurrentPage($currentPage)
		{
			$this->currentPage = $currentPage;
		}
		public function getCurrentPage()
		{
			return $this->currentPage;
		}
		public function setModule($module)
		{
			$this->module = $module;
		}
		public function getModule()
		{
			return $this->module;
		}

		//methods
		public function __construct()
		{
			//
		}

		public function render($name)
		{
			$file = 'view/'.$name.'.php';
			if(file_exists($file)) { include($file); }
		}
	}

?>
