<?php

    class Connection extends PDO
    {
        private $dsn;
        private $dataBase;
        private $user;
        private $pass;
        private $host;
        private $connect;

        public function __construct()
        {
        	$this->GetConfig();
        }

        public function ConnectDb()
        {
            if(!isset($this->connect))
            {
    	        try
    	        {
    	        	$this->connect = new PDO("mysql:host={$this->host};dbname={$this->dataBase};charset=".CHARSET, $this->user, $this->pass);
    	        	$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $this->connect->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES '.CHARSET);
    	        	return $this->connect;
    	        }
    	        catch (PDOException $error)
    	        {
    	        	echo '(Code CN001) Falha ao conectar ao banco de dados. Contacte Urgentemente o suporte.';
                    if(DEBUG) { echo $error; }
    	        }
            }
        }

        private function GetConfig()
        {
        	$this->dsn		= DSN;
        	$this->host 	= HOST;
        	$this->dataBase = DB;
        	$this->user 	= USER;
        	$this->pass 	= PASS;
        }
    }
