<?php
	class Controller
	{
		protected $module;
		protected $model;

		function __construct()
		{
	 		$this->view 	= new view();
	 		$this->baseURL	= BASE_URL;
		}

	    public function loadModel($name)
	    {
	    	$file = 'model/'.$name.'Model.php';

			if(file_exists($file))
			{
				require_once($file);

		        $modelname 		= $name.'Model';
		        $this->model 	= new $modelname();
			}
		}

		public function redirect($path)
		{
			header('Location: '.$path);
		}
	}
