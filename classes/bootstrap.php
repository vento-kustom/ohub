<?php
	class Bootstrap
	{
		private $module;
		private $page;
		private $method;
		private $param;

		//get & set
		public function setPage($page)
		{
			$this->page = $page;
		}
		public function getPage()
		{
			return $this->page;
		}
		public function setMethod($method)
		{
			$this->method = $method;
		}
		public function getMethod()
		{
			return $this->method;
		}

		//methods
		public function __construct($request)
	    {
			$split  = explode('/', $request);

			$this->page = $split[1];
	    	$this->page = str_replace('.php',   '', $this->page);
	    	$this->page = str_replace('.html',  '', $this->page);
	    	$this->page = str_replace('.aspx',  '', $this->page);
	    	$this->page = str_replace('.asp',   '', $this->page);
	    	$this->page = str_replace('.c', 	'', $this->page);
	    	$this->page = str_replace('.cs', 	'', $this->page);

			$controllerName = (empty($this->page)) ? 'indexController' : $this->page.'Controller' ;

	        $model 	= $this->page;
		    $file 	= 'controller/'.$controllerName.'.php';
			$method = (!empty($split[2])) ? $split[2] : null ;

			if(file_exists($file))
			{
				require($file);
		        $controller = new $controllerName($this->module);
		        $controller->loadmodel($model);

				if(!empty($method))
					$controller->$method();
				else
					$controller->show();
			}
			else
			{
				include('view/404.php');
			}
	    }
	}
