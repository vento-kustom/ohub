<?php

	include("connection.php");

	class Model extends connection
	{
		protected $connection;

		public function __construct()
		{
			parent::__construct();
			$this->connection = $this->connectDb();
		}

		protected function insert($table, $array)
	    {
	        try
	        {
				$array 		= $this->sanitize($array);
	        	$sqlInsert  = "INSERT INTO $table ({$array['fields']}) VALUES ({$array['data']});";
	        	$this->connection->query($sqlInsert);
	        	return $this->getLatID();
	        }
	        catch(PDOException $error)
	        {
	        	echo "(Código 120) Falha ao enviar os dados para o banco de dados. Erro de Insert, por favor contacte o suporte.";
				if(DEBUG) { echo $error; }
	        }

	    }

	    protected function update($table, $array, $id)
	    {
	    	try
	        {
		        /** Get fields of data base **/
				$data = null;
				foreach($array as $key => $value)
		        {
		            $data .= $key.' = "'.tools::secureString($value).'", ';
		        }
				$data .= 'updateAt = "'.date('Y-m-d H:i:s').'"';
				$id    = tools::secureString($id);

	        	$sqlUpdate  = "UPDATE $table SET $data WHERE id = '$id';";
	        	$this->connection->query($sqlUpdate);
	        	return $id;
	        }
	        catch(PDOException $error)
	        {
	        	echo "(Código 121) Falha ao enviar os dados para o banco de dados. Erro de Update, por favor contacte o suporte.";
				if(DEBUG) { echo $error; }
	        }
	    }

	    protected function delete($table, $id)
	    {
	    	try
	        {
				$deleteAt 	= 'deleteAt = "'.date('Y-m-d H:i:s').'"';
	 			$sqlDelete  = "UPDATE $table SET  {$deleteAt} WHERE id = '$id';";
	        	$this->connection->query($sqlDelete);
	        	return true;
	        }
	        catch(PDOException $error)
	        {
	        	echo "(Código 122) Falha ao enviar os dados para o banco de dados. Erro de Delete, por favor contacte o suporte.";
				if(DEBUG) { echo $error; }
	        }
	    }

	    protected function select($table, $fields = null, $params = null, $pagination = null)
	    {
	    	try
	    	{
				$fields  	= $this->sanitizeFields($fields);
				$where      = $this->sanitizeWhere($params);
				$limit 		= (sizeof($pagination)) ? 'LIMIT '.$pagination[0].', '.$pagination[1] : null ;

	    		$sqlSelect  = "SELECT $fields FROM $table WHERE deleteAt IS NULL $where $limit;";
				$result		= $this->connection->query($sqlSelect);

				return $result->fetchAll(PDO::FETCH_ASSOC);
	    	}
	    	catch(PDOException $error)
	    	{
	    		echo "(Código 123) Falha ao enviar os dados para o banco de dados. Erro de Select, por favor contacte o suporte.";
				if(DEBUG) { echo $error; }
	    	}
	    }

	    public function count($table, $field, $params = null)
	    {
	    	try
	    	{
				$where      = $this->sanitizeWhere($params);
	    		$sqlSelect  = "SELECT COUNT($field) AS NumRows FROM $table WHERE deleteAt IS NULL $where ;";
				$result 	= $this->connection->query($sqlSelect);
				$data		= $result->fetchAll(PDO::FETCH_ASSOC);

				return $data[0]['NumRows'];
	    	}
	    	catch (PDOException $error)
	    	{
	    		echo "(Código 124) Falha ao enviar os dados para o banco de dados. Erro de Count, por favor contacte o suporte.";
				if(DEBUG) { echo $error; }
	    	}
	    }

	    Public function getLatID()
	    {
	    	return $this->connection->lastInsertId();
	    }

	    protected function getConnection()
	    {
	    	return $this->connection;
	    }

		protected function sanitize($array)
		{
			$i 		= 1;
			$fields = $data = null;

			foreach($array as $key => $value)
			{
				$fields .= tools::secureString($key);
				$data   .= '"'.tools::secureString($value).'"';
				$data   .= ($i   < sizeof($array)) ? ', ' : null ;
				$fields .= ($i++ < sizeof($array)) ? ', ' : null ;
			}
			return array('fields' => $fields, 'data' => $data);
		}

		protected function sanitizeFields($fields = null)
		{
			if(!empty($fields))
			{
				$i 		= 1;
				$string = null;

				foreach($fields as $key => $value)
				{
					$string .= tools::secureString($value);
					$string .= ($i++ < sizeof($fields)) ? ', ' : null ;
				}
			}
			else
			{
				$string = '*';
			}

			return $string;
		}

		protected function sanitizeWhere($params)
		{
			$string = null;

			if(!empty($params))
			{
				$i 		= 1;
				$string = 'AND ';

				foreach($params as $key => $value)
				{
					$key.' => '.$value;
					$string .= tools::secureString($key).' = "'.tools::secureString($value).'"';
					$string .= ($i++ < sizeof($params)) ? ' AND ' : null;
				}
			}
			return $string;
		}
	}
	?>
