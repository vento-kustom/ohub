<?php
	include('class.phpmailer.php');

	class Tools extends Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public static function genUUID()
		{
			return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff),
																	mt_rand(0, 0xffff),
																	mt_rand(0, 0x0fff) | 0x4000,
																	mt_rand(0, 0x3fff) | 0x8000,
																	mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
		}

		public static function getNumber($string)
		{
			return preg_replace("/[^0-9]/", "", $string);
		}

		public static function getLetter($string)
		{
			return preg_replace("/[^A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]/", "", $string);
		}
		/**
		 * @param string $path caminho a ser redirecionado
		 */
		public static function redirect($path)
		{
			header("Location: $path");
		}

		/**
		 * @param string $msg mensagem a ser gerada antes do direcionamento
		 * @param string $path caminho a ser redirecionado
		 */
		public static function messageRedirect($msg, $path)
		{
			?><script>
				alert("<?=$msg?>");
				document.location.href="<?=$path?>";
			</script><?php
		}

		/**
		 * @param string $string texto a ser limpo pela rotina
		 */
		public static function clearString($string)
		{
			return preg_replace(array( 	"/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/",    "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/",
											"/(Í|Ì|Î|Ï)/",   "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/",  "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/",
											"/(ç)/", "/(Ç)/", "/(ñ)/", "/(Ñ)/", "/(#|=|\*|\\|\/|'|`|~| )/"),
											explode(" ","a A e E i I o O u U c C n N "),
											$string);
		}

		/**
		 * @param string $string texto a ser limpo pela rotina
		 */
		public static function secureString($string)
		{
			return preg_replace(array("/(#|=|\*|\\|\/|'|`|~|)/"), explode(" "," "), $string);
		}

		/**
		 * @param string $name referente ao nome e id do campo
		 * @param string $table nome da tabela a ser coletados os dados
		 * @param array  $fields campos a ser retornados do campo 0 referente ao Valeu do campo 1 o t�tulo que ser� exibido no campo
		 * @param string $selected dado a ser selecionado em caso de form de edi��o
		 */
		public static function comboBox($name, $table, $fields = Array(), $selected)
		{
			$result = $this->select($table, $fields);

			?><select id="<?=$name?>" name="<?=$name?>"><?php

				for($i=0; $i < sizeof(result); $i++)
				{
					?><option value="<?=$result[$i][$field[0]]?>"><?=$result[$i][$field[1]]?></option><?php
				}

			?></select><?php
		}

		/**
		 * @param string $text
		 * @param integer $limit
		 * @param boolean $breakLine
		 * @return Ambigous string
		 */
		public static function readMore($text, $limit = 30, $breakLine = false)
		{
			$size = strlen($text);

			if($size <= $limit)
			{
				$newText = $text;
			}
			else
			{
				if($breakLine)
				{
					$newText = trim(substr($text, 0, $limit))."...";
				}
				else
				{
					$lastSpace 	= strrpos(substr($text, 0, $limit), " ");
					$newText 	= trim(substr($text, 0, $lastSpace))."...";
				}
			}
			return $newText;
		}

		public static function monthName($month = 0)
		{
			$monthArray = Array( 0 => "",
								 1 => "Janeiro",
								 2 => "Fevereiro",
								 3 => "Março",
								 4 => "Abril",
								 5 => "Maio",
								 6 => "Junho",
								 7 => "Julho",
								 8 => "Agosto",
								 9 => "Setembro",
								10 => "Outubro",
								11 => "Novembro",
								12 => "Dezembro" );

			return $monthArray[intval($month)];
		}

		public static function sendMailPHP($to, $subject, $message, $from)
		{
			$headers    = "MIME-Version: 1.1\n";
			$headers   .= "Content-type: text/html; charset=utf-8\n";
			$headers   .= "From: Vento Macedo <no-reply@ventomacedo.com.br>\n";
			$headers   .= "Return-Path: Vento Macedo <no-reply@ventomacedo.com.br>\n";
			$headers   .= "Bcc: Vento Macedo <contato@ventomacedo.com.br>\n";

			return mail($to, $subject, $message, $headers);
		}

		public static function sendMailSMTP($to, $name, $subject, $message = null, $template = null)
		{
			$mail = new PHPMailer(true);
			$mail->IsSMTP();

			try
			{
				$mail->Host 	  = SMTP;
				$mail->Port       = PORT;
				$mail->Username   = EMAIL;
				$mail->Password   = EPASS;

				$mail->SMTPAuth   = true;
  				$mail->SMTPSecure = PROTOCOL;

				if(DEBUG)
					$mail->SMTPDebug  = 2;
				else
					$mail->SMTPDebug  = 0;

				$mail->FromName = TITLE;
				$mail->From	    = EMAIL;
			    $mail->Subject  = $subject;

				$mail->AddAddress($to, $name);

				/** Pega o template **/
				$mail->IsHTML(true);
				$mail->MsgHTML($message);

				$result = $mail->Send();
				$mail->ClearAllRecipients();
				$mail->ClearAttachments();

				return $result;
	    	}
			catch (phpmailerException $error)
			{
				echo '(Code EM001) Falha ao enviar o e-mail. Por gentileza, contacte o suporte.';
	      		if(DEBUG) { echo $error->errorMessage(); }
			}
		}

		public static function exportCSV($array)
		{
			$file   = 'clientes-cadastrados-'.date('ymd-his').'.csv';
			$output = fopen('php://output', 'w');

			fputcsv($output, array_keys($array[0]));
			foreach($array as $key => $value)
			{
				fputcsv($output, $value);
			}

			header ('Cache-Control: no-cache, must-revalidate');
			header ('Pragma: no-cache');
			header ('Content-Type: text/csv; charset=UTF-8');
			header ('Content-Disposition: attachment; filename='.$file);

			ob_end_flush();
		}
	}
