<?php
	/**
	 * Arquivo de configuração do sistema com as definições de constantes, idioma e demais informações relacionadas
	 * ao funcionamento do sistema. Criado com o intuido de centralização e facilidade para migrações e demais rotinas.
	 */

	/** Define o Charset Padrão **/
	header('Content-Type: text/html; charset=utf-8');

	/** Define o timezone do servidor **/
	date_default_timezone_set('America/Sao_Paulo');

	/** Base URL do sistema **/
	define('BASE_URL', 	'http://'.$_SERVER["HTTP_HOST"]);

	/** Define se exibe as mensagens de erro nativas **/
	define('DEBUG',	true);

	/** Número de registros por linha **/
	define('NUM_ROWS', 10);

	/** Define o charset para a conexão do banco **/
	define('CHARSET', 'utf8');

	/** Banco de dados **/
	define('DSN', 	'PDO_MYSQL');
	define('HOST',	'10.0.5.5');
	define('DB',	'db_teste_ohub');
	define('USER',	'root');
	define('PASS',	'secret');

	/** Dados de conexão de e-mail **/
	define('SMTP', 		'');
	define('PORT', 		'');
	define('EMAIL', 	'');
	define('EPASS', 	'');
	define('PROTOCOL',  '');
	define('TITLE', 	'');
?>
