# ************************************************************
# Sequel Pro SQL dump
# Vers�o 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 10.0.5.5 (MySQL 5.5.5-10.1.21-MariaDB-1~trusty)
# Base de Dados: db_teste_ohub
# Tempo de Gera��o: 2017-03-08 14:41:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela tb_token
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tb_token`;

CREATE TABLE `tb_token` (
  `token` char(36) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `score` double NOT NULL,
  `address` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(16) NOT NULL,
  `insertAt` datetime NOT NULL,
  `updateAt` datetime DEFAULT NULL,
  `deleteAt` datetime DEFAULT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tb_token` WRITE;
/*!40000 ALTER TABLE `tb_token` DISABLE KEYS */;

INSERT INTO `tb_token` (`token`, `email`, `score`, `address`, `ip`, `insertAt`, `updateAt`, `deleteAt`)
VALUES
	('0141bdd2-5774-4681-81b1-26e3e8c51eff','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:23',NULL,NULL),
	('06b8112d-c164-43ba-b2d1-4430afc07a38','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:09',NULL,NULL),
	('1413a1d9-4cbd-456f-a233-4b26ea5dfd5b','rafa.osbourne@gmail.com',0.8,'Rua Dom Pedro II, 93','10.0.5.1','2017-03-08 01:03:39',NULL,NULL),
	('186525f6-f260-45e0-9e01-2fd0e75e1a89','ecosocial@agenciatadah.com.br',0.96,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:28',NULL,NULL),
	('20646c2b-ec4f-4578-ba6a-133f9950e03a','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:13',NULL,NULL),
	('2208bd8b-c185-4d7f-9ac9-69734cd908cc','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:25',NULL,NULL),
	('22f2ec46-8476-4cef-a4ea-106f1880ceb9','rafa_osbourne@hotmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:44',NULL,NULL),
	('231dd7b5-db91-4768-95ad-50c3ab354cba','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:16',NULL,NULL),
	('2444c385-3488-435a-8c13-2dc08cee362b','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:39',NULL,NULL),
	('2849612d-98fa-4980-a68f-6c4cbfb2e184','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:13',NULL,NULL),
	('33e03f7d-967a-42da-8e12-899ab97182da','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:20',NULL,NULL),
	('340826ac-d85a-4ff1-944d-8224a3fefa54','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:11',NULL,NULL),
	('35079af9-ed65-450f-853d-ced2fb3d5369','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:03',NULL,NULL),
	('3a75c4bc-12ca-47d2-b90a-952b756a4d51','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-07 21:03:39',NULL,NULL),
	('40a65f5f-aa98-43b6-904e-8e3a1d9c1793','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:53',NULL,NULL),
	('4b7db948-9459-42ec-b9da-bb7af966e508','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:08',NULL,NULL),
	('4d6b7453-d353-447a-a714-c246b2b45a8b','rafa.osbourne@gmail.com',0.8,'Rua João de P. Guimarães, 70','10.0.5.1','2017-03-08 00:03:54',NULL,NULL),
	('5fd6a3c8-ad99-40fd-a4ef-d630fd26a066','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-07 21:03:08',NULL,NULL),
	('663df8e0-248e-4470-b4e5-a866e392020c','ecosocial@agenciatadah.com.br',0.96,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 10:03:59',NULL,NULL),
	('66824e90-e6fc-4751-81c5-45a0ed576fa0','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-07 21:03:55',NULL,NULL),
	('6e8e6763-7599-4363-a800-af6a73b03a3c','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:44',NULL,NULL),
	('74225e03-e77e-49bf-a41b-bb40499a5312','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:13',NULL,NULL),
	('7c103c11-6cb4-45fd-a66a-657e04109d08','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:37',NULL,NULL),
	('809095f0-3871-4eb9-93cc-672e7aee046f','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:55',NULL,NULL),
	('81ae28b7-757e-4ab3-80a9-b6a08c6f572c','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-07 21:03:14',NULL,NULL),
	('8598f092-08b0-451a-8e99-42195d49644d','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:18',NULL,NULL),
	('8c788bab-4273-430b-bf49-7738e571555b','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:49',NULL,NULL),
	('92ff2e16-91c8-441b-9835-8145a3db007f','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:50',NULL,NULL),
	('941de54d-c70c-4de1-8d43-8a00d1febe42','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-07 22:03:38',NULL,NULL),
	('9fb52f1d-9f55-44f0-b5dc-543ff760d7f7','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:18',NULL,NULL),
	('a3fc7f04-7dbf-4852-a36a-0fed80dd79d1','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus,107','10.0.5.1','2017-03-07 21:03:33',NULL,NULL),
	('ae509b09-f72f-44f3-bc9b-f838ea9292f5','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:59',NULL,NULL),
	('b65d4a2b-7837-47d3-a1f0-55a9642853ab','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:39',NULL,NULL),
	('c4c047db-f3b1-48bd-aa4b-75af0bb17fab','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:12',NULL,NULL),
	('cc067b9a-82a3-4e4f-9ad2-7a4077d9de1f','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:25',NULL,NULL),
	('de1c8964-f54f-4e6f-864e-28e774d402ff','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-07 21:03:23',NULL,NULL),
	('df495457-7802-413c-bf41-8b45c1137a08','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:45',NULL,NULL),
	('e8913f6a-7dc4-4b06-9773-ac3307c5c6be','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:06',NULL,NULL),
	('eaac04f9-de45-4bf4-9acb-c3d28872c2a1','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 01:03:33',NULL,NULL),
	('f62aa8fe-0eaa-458b-9be9-2e7fee53c2c6','rafa.osbourne@gmail.com',0.8,'Rua Calixto de Jesus, 107','10.0.5.1','2017-03-08 00:03:41',NULL,NULL);

/*!40000 ALTER TABLE `tb_token` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
