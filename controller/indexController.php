<?php

	class IndexController extends Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->loadModel('sucesss');
		}

		public function show()
		{
			$this->view->render('index');
		}

		public function go()
        {
			if(!session_id())
				session_start();

			$token 				= tools::genUUID();
			$array				= $_POST;
            $_SESSION['token']  = $array['token'] = $token;

			if(!isset($_COOKIE['cookie_token']))
				setcookie('cookie_token', 'CK-'.$token, time() + 3600, '/');

			$this->model->insertData($array);

			$dir   	= realpath( __DIR__ .'/..');
			$path 	= $dir.'/email/success.html';
			$html  	= file_get_contents($path);
			$html 	= str_replace('{{TOKEN}}', $_SESSION['token'], $html);

			if(tools::sendMailSMTP($_POST['email'], 'oHub - Vento Macedo', 'Obrigado por acessar meu teste!', utf8_decode($html)))
				tools::redirect('/success');
        }
	}
