<?php

	class SuccessController extends Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->loadModel('success');
		}

		public function show()
		{
			if(!session_id()) session_start();
			$this->view->setData($this->model->getList($_SESSION['token']));
			$this->view->render('success');
		}
	}
