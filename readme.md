# Teste de admissão para oHub

Teste realizado para demonstração dos conhecimentos em PHP, HTML5, CSS3, SASS, JQUERY, JavaScript, MySQL, MVC e OOP.

#### Funções:
* Esse software faz a consulta da validade e existência do e-mail por intermédio da API Mailboxlayer;
* Tratamento de string por REGEX para verificar número predial;
* Layout responsivo utilizando Bootstrap;
* Gera um UUID como token de uso;
* Maneijo de Cookies;
* Trabalho MVC e OOP;
* Guarda os dados via banco de dado:
    * Token gerado;
    * E-mail;
    * Score do e-mail (API Mailboxlayer);
    * Endereço comercial;
    * Número predial;
    * IPV4;
    * Data de cadastro.
